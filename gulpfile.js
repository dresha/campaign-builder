(function(){
    // Modules
    var gulp = require('gulp');
    var tap = require('gulp-tap');
    var concat = require('gulp-concat');
    var wrap = require('gulp-wrap');
    var minimist = require('minimist');
    var fileGulp = require('gulp-file');
    var fs = require('fs');
    var path = require('path');
    var addsrc = require('gulp-add-src');

    // Campaign variable
    var pathSandbox = './Sandbox/'; // './Production/'
    var pathDev = './Dev/';

    // Call command line with arguments
    var knownOptions = {
        string: 'camp',
        default: { camp: process.env.NODE_ENV || '$nameCampaign$' }
    };
    var options = minimist(process.argv.slice(2), knownOptions);

    // Additional functions
    function getDirectories(srcpath) {
        return fs.readdirSync(srcpath).filter(function(file) {
            return fs.statSync(path.join(srcpath, file)).isDirectory();
        });
    }

    function convertElementsName (arr){
        var names = "";
        arr.forEach(function(elem){
            names += "'" + elem + "',";
        });
        return "[" + names.replace(/,$/, "]");

    }

    gulp.task('build-test', function() {
        var campaignName = options.camp;
        var campPath = pathDev + campaignName + '/';
        var varHTML = campPath + '*/*/*.html';
        var pathSandbox = './Production/' + campaignName + '/';

        gulp.src(varHTML)
            .pipe(tap(function(file){
                var path = file.path;
                var pathNames = path.split('\\');
                var html = file.contents.toString();
                var nameVariant = pathNames[pathNames.length-2];
                var elementName = pathNames[pathNames.length-3];

                // Check mm_inner_HTML is empty
                if(html === "<!-- Empty HTML -->"){
                    html = '';
                }

                BuildScript();

                // Open and build script
                function BuildScript(){
                    gulp.src(path.replace('.html', '.js'))
                        .pipe(tap(function(file){
                            var variantContent = file.contents.toString();
                            // Add script to variant
                            var scriptNames = variantContent.match(/\/\/\s*include:_scripts\s*\[(.*)\]/);
                            var variantStream;

                            if(variantContent == "// Empty JS"){
                                // For empty script
                                variantStream = fileGulp(nameVariant + '.html', html)
                                    .pipe(gulp.dest(pathSandbox + elementName));
                            }else if(scriptNames && scriptNames.length === 2){
                                // Needed add modules into script
                                var needLoad = scriptNames[1].replace(/\s/g, "").split(',');

                                needLoad = needLoad.map(function(nameFunction){
                                    return campPath + '_scripts/' + nameFunction + '.js';
                                });

                                // Read all need files
                                variantStream = gulp.src(needLoad)
                                    .pipe(concat('test.js'))
                                    .pipe(wrap(html + '<scr'+'ipt>\n(function(){\n'+ variantContent + '\n<%= contents %>\n}());\n</script>'))
                                    .pipe(concat(nameVariant + '.html'))
                                    .pipe(gulp.dest(pathSandbox + elementName));
                            }else {
                                // Script without modules
                                variantStream = fileGulp(nameVariant + '.html', '<scr'+'ipt>\n'+ variantContent + '\n</script>' )
                                    .pipe(gulp.dest(pathSandbox + elementName));
                            }

                            // Scripts and html is append
                            variantStream.on('end', function(){
                                console.log('try build css');
                                buildCSS(nameVariant, elementName);
                            });
                        }))
                }

                // Append styles
                function  buildCSS(nameVariant, elementName){
                    console.log(campPath + elementName + '/' + nameVariant + '/' + nameVariant + '.css');
                    gulp.src(campPath + elementName + '/' + nameVariant + '/' + nameVariant + '.css')
                        .pipe(tap(function(file){
                            var css = file.contents.toString();
                            var includeCss = css.match(/\/\*\s*include:css\s*\[(.*)\]/);
                            var needLoad;

                            console.log(css);

                            // Not any styles in variant
                            if(css == "/* Empty CSS */"){
                                return;
                            }else if(includeCss && includeCss.length === 2){

                                needLoad = includeCss[1].replace(/\s/g, "").split(',');

                                needLoad = needLoad.map(function(nameFunction){
                                    return campPath + '_styles/' + nameFunction + '.css';
                                });

                                gulp.src(needLoad)
                                    .pipe(concat('test.css'))
                                    .pipe(wrap(html + '<style>\n'+ css + '<%= contents %>\n</style>'))
                                    .pipe(addsrc(pathSandbox + elementName + '/' + nameVariant + '.html'))
                                    .pipe(concat(nameVariant + '.html'))
                                    .pipe(gulp.dest(pathSandbox + elementName));
                            }
                        }))
                }
            }))
    });

    gulp.task('build', function() {
        var path;

        for(path in paths){
            gulp.src(paths[path]).pipe(tap(function(file){
                var variantContent = file.contents.toString();
                var scriptToVariant;

                // Add script to variant
                var scriptNames = variantContent.match(/\/\/\s*mm-add-script\s*\[(.*)\]/);

                if(scriptNames && scriptNames.length === 2){
                    var needLoad = scriptNames[1].replace(/\s/g, "").split(',');

                    needLoad = needLoad.map(function(nameFunction){
                       return baseURL + '_scripts/' + nameFunction + '.js';
                    });

                    // Read all need files
                    return gulp.src(needLoad)
                        .pipe(concat('test.js'))
                        .pipe(wrap('<scr'+'ipt>\n(function(){\n'+ variantContent + '\n<%= contents %>\n}());\n</script>'))
                        .pipe(concat('test.html'))
                        .pipe(gulp.dest(pathSandbox));
                }
            }));
        }
    });

    gulp.task('create', function() {
        var campaignName = options.camp;
        var elementsPath = getDirectories(pathSandbox + campaignName);
        var elementsName = convertElementsName(elementsPath);
        var campElem = pathSandbox + campaignName + '/*';

        if(campaignName === "$nameCampaign$"){
            return;
        }

        fs.mkdir(pathDev + campaignName, function(){
            createFoldersForTemplates();
        });

        function createFoldersForTemplates(){
            var folders = ["/_scripts", "/_styles", "/_templates"];
            // Create folders for templates
            folders.forEach(function(nameFolder){
                var pathTemplate = pathDev + campaignName + nameFolder;

                fs.mkdir(pathTemplate, function(){
                    if(nameFolder === '/_scripts'){

                        // Write render file
                        gulp.src('./render.js')
                            .pipe(tap(function(file){
                                var render = file.contents.toString();
                                var renderJs;
                                renderJs = render.replace(/CampaignName/g, campaignName)
                                    .replace(/ElementsName/g, function () {
                                        return elementsName;
                                    })
                                    .replace(/\'\[/g, '[')
                                    .replace(/\]\'/g, ']');

                                fs.writeFile(pathTemplate + '/render.js', renderJs, function(){
                                    console.log('render script done');
                                    parseCampaignElements().on('end', function() {
                                        //run some code here
                                        console.log('Set campaign elements');
                                        separateVariant();
                                    });

                                })
                            })
                        );
                    }
                });
            });
        }

        function parseCampaignElements(){
            return gulp.src([campElem])
                .pipe(gulp.dest(pathDev + campaignName));
        }

        function separateVariant(){
            elementsPath.forEach(function (elemName) {
                var pathToElement = campaignName + '/' + elemName;

                gulp.src(pathSandbox + pathToElement + '/*.html')
                    .pipe(tap(function (file) {
                        var variantContent = file.contents.toString();
                        var variantName = file.relative.replace('.html', '');
                        var files = {
                            '.js': {
                                content: variantContent.match(/<script>([\s\S]*)<\/script>/),
                                empty: '// Empty JS'
                            },
                            '.css': {
                                content: variantContent.match(/<style>([\s\S]*)<\/style>/),
                                empty: '/* Empty CSS */'
                            },
                            '.html': {
                                content: variantContent.replace(/<script>[\s\S]*<\/script>/g, '')
                                    .replace(/<style>[\s\S]*<\/style>/g, ''),
                                empty: '<!-- Empty HTML -->'
                            }
                        };

                        var dir = pathDev + pathToElement + '/' + variantName;

                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);
                        }

                        // Create .js .css .html
                        for(var typeFile in files){
                            (function(type, data){
                                var content = data.content;
                                var filePath = pathDev + pathToElement + '/' + variantName + '/' + variantName + type;
                                var fileContents = data.empty;

                                // If file not empty
                                if(content && content.length == 2){
                                    console.log('file not empty');
                                    fileContents = content[1];
                                }

                                buildFile(filePath, fileContents, {name: variantName, type: type});
                            }(typeFile, files[typeFile]));
                        }

                        function buildFile(fileName, fileContent, CBdata) {
                            // Build files for variants
                            fs.writeFile(fileName, fileContent, function () {
                                console.log(CBdata.type + ' for variant ' + CBdata.name + ' complete');
                            })
                        }
                    })
                );
            });
        }

    });

}());

