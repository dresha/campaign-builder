(function(mmcore) {
    var campaign;

    campaign = new mmcore.Campaign('CampaignName','ElementsName', 't1');

    campaign.hideContent('.test-area');

    mmcore.waitFor(function(){
        return true;
    })
    .then(function() {
        return mmcore.request('CampaignName');
    })
    .done(function() {
        campaign.renderMaxyboxes();
    })
    .always(function() {
        campaign.showContent();
    });
}(window.mmcore));